const path = require('path')
const commandLineArgs = require('command-line-args')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const TSLintPlugin = require('tslint-webpack-plugin')


let argv = commandLineArgs([{ name: 'mode', type: String, defaultValue: 'development' },
{ name: 'environment', type: String, defaultValue: 'dev' },
{ name: 'platform', type: String, defaultValue: 'desktop' },
{ name: 'target-src', type: String, defaultValue: 'build/undefined' }],
	{ partial: true })



// Extension by enviroments ---------------------------------------

let extensions = ['tsx', 'ts', 'js', 'scss']
let calcResolves = () => {
	let environment = argv.environment
	let platform = argv.platform

	let res = []
	for (let ext of extensions) {
		res.push('.' + platform + '.' + environment + '.' + ext)
		res.push('.' + environment + '.' + ext)
		res.push('.' + platform + '.' + ext)
		res.push('.' + ext)
	}

	return res
}



// Webpack config -----------------------------------------------

let first = true
let isProduction = argv.mode == 'production'
const config = (entries, target, htmlFile, vendors, inline) => {

	let config = {
		mode: isProduction ? 'production' : 'development',
		target,
		resolve: {
			extensions: calcResolves(),
			modules: [path.resolve(__dirname, 'src'), 'node_modules']
		},
		context: __dirname,
		output: {
			path: path.join(__dirname, argv['target-src']),
			filename: '[name].js',
			publicPath: ''
		},
		devServer: {
			host: '0.0.0.0',
			port: inline ? 8080 : 8081,
			inline
		},
		node: false,
		module: {
			rules: [
				{
					test: /\.scss$/,
					exclude: /node_modules/,
					use: [
						'style-loader',
						'css-loader',
						{
							loader: 'sass-loader',
							options: {
								outputStyle: 'compressed'
							}
						}
					]
				},
				{
					test: /\.tsx?$/,
					use: 'ts-loader',
					exclude: /node_modules/
				}
			]
		},
		optimization: {
			minimizer: []
		}
	}


	config.plugins = []
	if (first && inline) {
		config.plugins.push(new CopyWebpackPlugin([
			{ from: 'src/assets', to: 'assets' }
		]))

		first = false
	}

	if (htmlFile != null && inline) {
		if (vendors) {
			config.optimization.splitChunks = {
				cacheGroups: {
					vendors: {
						test: /[\\/]node_modules[\\/]/,
						name: 'vendors',
						enforce: true,
						chunks: 'all'
					}
				}
			}
		}

		config.plugins = [
			new HTMLWebpackPlugin({
				filename: `${htmlFile}.html`,
				template: `./src/${htmlFile}.html`,
				inject: true
			}),

			new MiniCssExtractPlugin({ filename: 'styles.css' })
		]
	}

	if (isProduction) {
		config.optimization.minimizer.push(new TerserPlugin())
		config.plugins.push(new TSLintPlugin({
			files: ['./src/**/*.ts', './src/**/*.tsx']
		}))
	}

	config.entry = {}
	for (let entry of entries) {
		config.entry[entry.name] = entry.entry
		config.output = Object.assign(config.output, entry.output)
	}

	if (inline)
		config.devServer.proxy = {
			"/pad": {
				target: "http://localhost:8081",
				pathRewrite: {"^/pad" : ""}
			}
		}

	return config;
}

module.exports = (env) => {

	let inline = true
	res = [
		config([{
			entry: './src/index.ts',
			name: 'index'
		}], 'electron-main', null, false, inline),
		config([{
			entry: './src/launcher-api/index.ts',
			name: 'launcher',
			output: {
				libraryTarget: 'umd',
				library: '[name]',
				umdNamedDefine: true
			}
		}], 'electron-renderer', null, false, inline),
		config([{
			entry: './src/launcher/app.tsx',
			name: 'app'
		}], 'electron-renderer', 'index', true, inline)
	]

	if (env && env.inline && env.inline == false) {
		res = []
		inline = false
	}

	res.push(
		config([{
			entry: './src/pad/index.ts',
			name: 'pad'
		}], 'web', 'pad', false, inline)
	)

	return res
}
