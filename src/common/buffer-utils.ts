
export class BufferUtils {

	public static copyBuffer(to: Buffer, from: Buffer, offset: number, offsetFrom: number = 0): Buffer {
		for (let i = 0; i < from.length && offset + i < to.length; i++)
			to.writeInt8(from.readInt8(offsetFrom + i), offset + i)

		return to
	}

	public static copyArray(to: Buffer, from: number[], offset: number, offsetFrom: number = 0): Buffer {
		for (let i = 0; i < from.length && offset + i < to.length; i++) {
			let byte = from[offsetFrom + i]
			if (byte > 127)
				byte = byte - 256
			to.writeInt8(byte, offset + i)
		}

		return to
	}

}
