
export class DataTypes {
	public static readonly typeBoolean: string 					= 'boolean'
	public static readonly typeInteger: string 					= 'int'
	public static readonly typeFloat: string 					= 'float'
	public static readonly typeString: string 					= 'string'


	private static readonly defaultValues: any = {
		boolean: false,
		int: 0,
		float: 0.0,
		string: '',
	}

	public static getDefaultValues(type: string, length: number): any[] {
		const res = new Array(length)
		for (let i = 0; i < length; i++)
			res[i] = DataTypes.defaultValues[type]

		return res
	}
}
