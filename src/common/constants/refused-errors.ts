
export class RefusedErrors {
	public static readonly UNKNOW_ERROR: number 	= 0x01
	public static readonly ENOUGH_DEVICES: number 	= 0x02
	public static readonly LACK_OF_SENSORS: number 	= 0x03
}
