
export class Sensors {
	public static readonly ACCELEROMETER: number 		= 0x01
	public static readonly GRAVITY: number 				= 0x02
	public static readonly GYROSCOPE: number 			= 0x03
	public static readonly LIGHT: number 				= 0x04
	public static readonly LINEAR_ACCELERATION: number 	= 0x05
	public static readonly MAGNETIC_FIELD: number 		= 0x06
	public static readonly LED: number 					= 0x07
	public static readonly LED_RGB: number 				= 0x08
	public static readonly SPEAKER: number 				= 0x09
	public static readonly VIBRATION: number 			= 0x0A
	public static readonly CAMERA: number 				= 0x0B
	public static readonly CAMERA_FRONT: number 		= 0x0C
	public static readonly MICROPHONE: number 			= 0x0D
	public static readonly KEYBOARD: number 			= 0x0E

	public static readonly ECHO_REQ: number 			= -1
	public static readonly ECHO_RES: number 			= 0x0
}
