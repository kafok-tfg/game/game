import { ipcRenderer } from 'electron'
import * as launcher from 'launcher-api'
import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { DataTypes } from './../common/constants/data-types'
import { LoginView } from './components/views/login/login-view'
import './global'
import { store } from './store'
import { changeButtons, changeDirections, resize } from './store/controls'
import { addButton, removeButton } from './store/pad-state'

// Configure internal protocol
launcher.virtualPadAPI.addFile('icon', '/assets/icon.png', 'image/png')
launcher.virtualPadAPI.addFile('controller',  'pad.js', 'text/ecmascript')

launcher.virtualPadAPI.setConverter(100, { dataType: DataTypes.typeBoolean, length: 1 })
launcher.virtualPadAPI.setConverter(101, { dataType: DataTypes.typeBoolean, length: 1 })
launcher.virtualPadAPI.setConverter(102, { dataType: DataTypes.typeBoolean, length: 4 })
launcher.virtualPadAPI.setConverter(103, { dataType: DataTypes.typeBoolean, length: 1 })
launcher.virtualPadAPI.setConverter(104, { dataType: DataTypes.typeBoolean, length: 1 })
launcher.virtualPadAPI.setConverter(105, { dataType: DataTypes.typeBoolean, length: 1 })
launcher.virtualPadAPI.setConverter(500, { dataType: DataTypes.typeString, length: 1 })

launcher.virtualPadAPI.setDataFrecuency(12)
launcher.virtualPadAPI.setName('LabcubY')

launcher.virtualPadAPI.addFileCallback('pad-state', (name) => {
	return new Promise((resolve) => {
		resolve({
			name,
			mimeType: 'application/json',
			data: Buffer.from(JSON.stringify(store.getState().padState), 'utf-8'),
		})
	})
})


// Fulscreen control
let isFullscreen = false


// Process on received controls
let oldDirecionsControls = { up: false, down: false, left: false, right: false }
let oldButtonsControls: any = {
	enter: false, back: false,
	close: false,
}
setInterval(() => {
	// Directions
	let resData = { up: false, down: false, left: false, right: false }
	for (const device of launcher.virtualPadAPI.getDevices()) {
		const data = launcher.virtualPadAPI.getData(102, device.address)

		resData.up = resData.up || data[0]
		resData.down = resData.down || data[1]
		resData.left = resData.left || data[2]
		resData.right = resData.right || data[3]
	}
	resData = resData

	if (resData.up != oldDirecionsControls.up ||
			resData.down != oldDirecionsControls.down ||
			resData.left != oldDirecionsControls.left ||
			resData.right != oldDirecionsControls.right) {
		store.dispatch(changeDirections(resData))
		oldDirecionsControls = resData
	}


	// Buttons
	const resButtonsControl: any = {
		enter: false, back: false,
		close: false,
		setFullscreen: false,
		exitFullscreen: false,
	}
	for (const device of launcher.virtualPadAPI.getDevices()) {
		resButtonsControl.enter = resButtonsControl.enter || launcher.virtualPadAPI.getData(100, device.address)[0]
		resButtonsControl.back = resButtonsControl.back || launcher.virtualPadAPI.getData(101, device.address)[0]
		resButtonsControl.setFullscreen = resButtonsControl.setFullscreen || launcher.virtualPadAPI.getData(103, device.address)[0]
		resButtonsControl.exitFullscreen = resButtonsControl.exitFullscreen || launcher.virtualPadAPI.getData(104, device.address)[0]
		resButtonsControl.close = resButtonsControl.close || launcher.virtualPadAPI.getData(105, device.address)[0]
	}

	let test = false
	for (const prop in resButtonsControl)
		if (resButtonsControl[prop] != oldButtonsControls[prop]) {
			test = true
			break
		}

	if (test) {
		if (resButtonsControl.setFullscreen) {
			delete resButtonsControl.setFullscreen
			ipcRenderer.send('config-backend-fullscreen', true)
			isFullscreen = true
			store.dispatch(removeButton('fullscreen'))
		} else if (resButtonsControl.exitFullscreen) {
			delete resButtonsControl.exitFullscreen
			ipcRenderer.send('config-backend-fullscreen', false)
			isFullscreen = false
			store.dispatch(addButton('fullscreen'))
		}

		store.dispatch(changeButtons(resButtonsControl))
		oldButtonsControls = resButtonsControl
	}

}, 50)


// Resize
window.addEventListener("resize", () => store.dispatch(resize({ width: window.innerWidth, height: window.innerHeight })))


// App
ReactDOM.render(
	<Provider store={store}>
		<LoginView></LoginView>
	</Provider>,
	document.getElementById('root'),
)
