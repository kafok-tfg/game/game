
export class RepeaterDirection {

	private timer: NodeJS.Timeout
	private readonly initSpeed: number
	private readonly maxSpeed: number
	private readonly acceleration: number
	private speed: number

	constructor(initSpeed: number, maxSpeed: number, acceleration: number) {
		this.initSpeed = initSpeed
		this.maxSpeed = maxSpeed
		this.speed = initSpeed
		this.acceleration = acceleration
	}


	public start(callback: () => void) {
		clearTimeout(this.timer)
		this.timer = setInterval(() => {
			callback()

			this.speed -= this.acceleration
			if (this.speed < this.maxSpeed)
				this.speed = this.maxSpeed

			this.start(callback)
		}, this.speed)
	}

	public stop() {
		clearTimeout(this.timer)
		this.speed = this.initSpeed
	}

}
