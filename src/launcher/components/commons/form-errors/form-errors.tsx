import * as React from 'react'
import './styles'

export class FormErrors extends React.Component<any, any> {

	constructor(props: any) {
		super(props)
	}


	public render(): React.ReactNode {
		return (
			<div className="form-errors">
				<ul>
					{ this.props.errors.map((error: any, i: any) => <li key={i}>{ error }</li>) }
				</ul>
			</div>
		)
	}

}
