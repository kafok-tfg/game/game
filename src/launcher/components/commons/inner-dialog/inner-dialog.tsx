import * as React from 'react'
import { store } from '../../../store'
import { closeDialog } from '../../../store/dialog'
import './styles'

export class InnerDialog extends React.Component<any, any> {

	constructor(props: any) {
		super(props)

		this.state = {
			width: this.props.width ? this.props.width : '40em',
			height: this.props.height ? this.props.height : 'auto',
		}

		this.click = this.click.bind(this)
		this.close = this.close.bind(this)
	}

	public click(event: any) {
		if (event.target == event.currentTarget)
			store.dispatch(closeDialog())
	}

	public close(event: any) {
		store.dispatch(closeDialog())
	}

	public render(): React.ReactNode {
		return (
			<div className="inner-dialog" onClick={this.click}>
				<div style={{ width: this.state.width, height: this.state.height }}>
					<div className="inner-dialog-header">
						{ this.props.title }
						<div className="close" onClick={this.close}>
							<i className="fas fa-times"></i>
						</div>
					</div>

					<div className="inner-dialog-content">
						{ this.props.children }
					</div>

					<div className="inner-dialog-submit"></div>
				</div>
			</div>
		)
	}

}
