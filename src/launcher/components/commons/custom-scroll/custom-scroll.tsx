import * as React from 'react'
import { connect } from 'react-redux'
import './styles'

class CustomScrollRaw extends React.Component<any, any> {

	private top: number
	private barSize: number
	private readonly bar: React.RefObject<HTMLDivElement>


	constructor(props: any) {
		super(props)

		this.top = 0
		this.barSize = 1
		this.state = {
			display: 'hidden',
		}

		this.wheel = this.wheel.bind(this)

		this.bar = React.createRef()
	}


	public wheel(e: any) {
		this.updateBar(e.currentTarget, e.deltaY)
	}

	private updateBar(elem: HTMLElement, deltaY: number, update = true) {
		// Control max and min
		const dir = Math.sign(deltaY)

		this.top += dir*70
		const max = elem.scrollHeight - elem.offsetHeight
		if (this.top < 0) this.top = 0
		if (this.top > max) this.top = max

		// Scroll
		elem.scroll({ left: 0, top: this.top })

		// Bar size
		const scrollBar: HTMLElement = elem.childNodes[elem.childNodes.length - 1] as HTMLElement
		this.barSize = elem.offsetHeight*1.0 / elem.scrollHeight*1.0
		const child: HTMLElement = scrollBar.childNodes[0] as HTMLElement

		child.style.height = Math.round(this.barSize*100) + '%'
		child.style.top = Math.round(this.top*(1 - this.barSize)/elem.offsetHeight) + 'px'

		if (update)
			this.setState({ display: Math.abs(this.barSize - 1) > 0.001 ? 'block' : 'none' })

		// Move bar
		scrollBar.style.top = this.top + 'px'
		child.style.top = Math.round(this.top/elem.scrollHeight*elem.offsetHeight) + 'px'
	}

	public componentWillReceiveProps(nextProps: any) {
		if (nextProps.top)
			this.top = nextProps.top
	}

	public componentDidUpdate() {
		this.updateBar(this.bar.current, 0, false)
	}

	public render(): React.ReactNode {
		return (
			<div onWheel={this.wheel} className={this.props.className + ' custom-scroll'} ref={ this.bar }>
				{ this.props.children }
				<div className="scroll-bar" style={{display: this.state.display}}>
					<div></div>
				</div>
			</div>
		)
	}

}


const mapStateToProps = (state: any) => {
	return {
		resize: state.resize,
	}
}

export const CustomScroll = connect(mapStateToProps)(CustomScrollRaw)
