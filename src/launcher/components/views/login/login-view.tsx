import * as fs from 'fs'
import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import { User } from '../../../models/users'
import { userService } from '../../../services/user-service'
import { store } from '../../../store'
import { closeDialog } from '../../../store/dialog'
import { FormErrors } from '../../commons/form-errors/form-errors'
import { InnerDialog } from '../../commons/inner-dialog/inner-dialog'
import { LoginHeaderComponent } from './header/login-header-component'
import './styles'
import { UsersComponent } from './users/users-component'

class LoginViewRaw extends React.Component<any, any> {

	constructor(props: any) {
		super(props)

		this.state = {
			dialog: {
				name: '',
				password: '',
				confirmPassword: '',
			},
			selected: -1,
			errors: [],
		}

		this.handleInputChange = this.handleInputChange.bind(this)
		this.handleSubmitNew = this.handleSubmitNew.bind(this)
		this.handleSubmitLogin = this.handleSubmitLogin.bind(this)
	}


	public componentWillReceiveProps(nextProps: any) {
		if (nextProps.newUserDialog || nextProps.loginUserDialog)
			this.setState({
				errors: [],
				dialog: {
					name: '',
					password: '',
					confirmPassword: '',
				},
			})

		if (nextProps.controls.directions && this.props.newUserDialog)
			if (this.state.selected <= 0)
				this.setState({ selected: 1 })
			else {
				let res: number = this.state.selected
				if (nextProps.controls.directions.up)
					res--

				if (nextProps.controls.directions.down)
					res++

				if (res < 1) res = 1
				if (res > 5) res = 5

				this.setState({ selected: res })
			}

		if (nextProps.controls.buttons)
			if ((nextProps.controls.buttons.back || nextProps.controls.buttons.close) && nextProps.newUserDialog)
				store.dispatch(closeDialog())
	}


	public handleInputChange(event: any) {
		const target = event.target

		if (target.name == 'avatar') {
			const file: any = (ReactDOM.findDOMNode(this) as HTMLElement).querySelector('input[type="file"]')
			const span = (ReactDOM.findDOMNode(this) as HTMLElement).querySelector('.input-avatar-container span')
			span.innerHTML = file.value.split(/(\\|\/)/g).pop()
		} else {
			const res = {...this.state.dialog}
			res[target.name] = target.value
			this.setState({
				dialog: res,
			})
		}

	}

	public handleSubmitNew(event: any) {
		event.preventDefault()

		const errors: string[] = []


		// Validaciones

		const name = this.state.dialog.name
		if (name.length < 4 || name != name.replace(/[^a-z0-9_]/gi, ''))
			errors.push('El nombre introducido debe contener al menos 4 caracteres alfanumericos o \'_\'')

		if (this.state.dialog.password.length < 3)
			errors.push('La contraseña debe medir mínimo 3 caracteres')

		if (this.state.dialog.password != this.state.dialog.confirmPassword)
			errors.push('Las contraseñas no coinciden')

		if (errors.length > 0) {
			const dialog = {...this.state.dialog}
			dialog.password = ''
			dialog.confirmPassword = ''
			this.setState({ errors, dialog })

			return
		}


		// Final task (create user)

		const user: User = new User()
		user.name = name
		user.password = this.state.dialog.password

		const createUser = () => {
			userService.createUser(user)
			.then(() => {
				store.dispatch(closeDialog())
			})
			.catch((err) => {
				const errs: string[] = []
				if (err == 'exists')
					errs.push('!El usuario ya existe!')
				else
					errs.push('Error al crear usuario')

				this.setState({ errors: errs })
			})
		}


		// Process avatar

		const file: any = (ReactDOM.findDOMNode(this) as HTMLElement).querySelector('input[type="file"]')
		if (file.files.length > 0) {
			const reader = new FileReader()
			reader.onload = (f) => {
				userService.setAvatar(user.name, f.target.result as string)
					.then(createUser)
					.catch(() => {
						this.setState({ errors: ['Error al leer archivo'] })
						createUser()
					})
			}
			reader.onerror = () => {
				this.setState({ errors: ['Error al leer archivo'] })
				createUser()
			}
			reader.readAsDataURL(file.files[0])
		} else
			createUser()
	}

	public handleSubmitLogin(event: any) {
		event.preventDefault()

		const errors: string[] = []

		userService.loginUser(this.props.dialogData.name, this.state.dialog.password)
			.then(() => {})
			.catch((err) => {
				switch (err) {
					case 'bad-password':
						errors.push('La contraseña introducida es incorrecta')
						break

					case 'exists':
						errors.push('!El usuario no existe!')
						break

					default:
						errors.push('Ha ocurrido un error interno')
						break
				}

				this.setState({ errors })
			})
	}


	public render(): React.ReactNode {
		return (
			<div className="login-view">
				<LoginHeaderComponent></LoginHeaderComponent>
				<UsersComponent></UsersComponent>

				{
					this.props.newUserDialog ?
						<InnerDialog title="Nuevo usuario">
							<form className="form" onSubmit={ this.handleSubmitNew }>
								{
									this.state.errors.length > 0 ?
										<FormErrors errors={ this.state.errors }></FormErrors>
									:
									''
								}
								<div className="form-group">
									<label className="required">Nombre de usuario</label>
									<input type="text" name="name" value={ this.state.dialog.name } onChange={ this.handleInputChange }
										className={ this.state.selected == 1 ? 'input-selected' : '' } />
								</div>
								<div className="form-group">
									<label className="required">Contraseña</label>
									<input type="password" name="password" value={ this.state.dialog.password } onChange={ this.handleInputChange }
										className={ this.state.selected == 2 ? 'input-selected' : '' } />
								</div>
								<div className="form-group">
									<label className="required">Repita la contraseña</label>
									<input type="password" name="confirmPassword" value={ this.state.dialog.confirmPassword } onChange={ this.handleInputChange }
										className={ this.state.selected == 3 ? 'input-selected' : '' } />
								</div>
								<div className="form-group">
									<label>Foto</label>
									<div className="input-avatar-container"
											style={{ margin: '0.8em', padding: '0.3em', overflow: 'hidden', whiteSpace: 'nowrap', textOverflow: 'ellipsis' }}>
										<label className="input-avatar button">
											Cargar
											<input type="file" name="avatar" onChange={ this.handleInputChange } accept="image/*"
												className={ this.state.selected == 4 ? 'input-selected' : '' } />
										</label>
										<span style={{ marginLeft: '0.5em'}}>None</span>
									</div>
								</div>
								<div className="center" style={{ marginTop: "1.3em", marginBottom: "0.5em" }}>
									<button className={ this.state.selected == 5 ? 'box-selected' : '' }>
										Añadir usuario
									</button>
								</div>
							</form>
						</InnerDialog>
						:
						''
				}
				{
					this.props.loginUserDialog ?
						<InnerDialog title="Contraseña" width="25em">
							<form className="form" onSubmit={ this.handleSubmitLogin }>
								{
									this.state.errors.length > 0 ?
										<FormErrors errors={ this.state.errors }></FormErrors>
									:
									''
								}
								<div className="form-group only-input">
									<input type="password" name="password" value={ this.state.dialog.password } onChange={ this.handleInputChange }
										className={ this.state.selected == 2 ? 'input-selected' : '' } />
								</div>
								<div className="center" style={{ marginTop: "1.3em", marginBottom: "0.5em" }}>
									<button className={ this.state.selected == 5 ? 'box-selected' : '' }>
										Ingresar
									</button>
								</div>
							</form>
						</InnerDialog>
						:
						''
				}
			</div>
		)
	}

}


const mapStateToProps = (state: any) => {
	return {
		controls: state.controls,
		newUserDialog: state.dialog.dialogs['new-user'],
		loginUserDialog: state.dialog.dialogs['login-user'],
		dialogData: state.dialog.data,
	}
}

export const LoginView = connect(mapStateToProps)(LoginViewRaw)
