import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import { User } from '../../../../models/users'
import { userService } from '../../../../services/user-service'
import { store } from '../../../../store'
import { openDialog } from '../../../../store/dialog'
import { RepeaterDirection } from '../../../../utils/repeater-direction'
import { CustomScroll } from '../../../commons/custom-scroll/custom-scroll'
import './styles'


class UsersComponentRaw extends React.Component<any, any> {

	private readonly repeater: RepeaterDirection

	private readonly loginUsers: React.RefObject<HTMLDivElement>
	private columns: number


	private static movePointer(newThis: UsersComponentRaw, nextProps: any) {
		let selected = newThis.state.selected
		if (selected < 0) {
			newThis.setState({ selected: 0 })
			return
		}

		// Move
		const max = newThis.state.users.length + 1
		if (nextProps.controls.directions.right && Math.floor(selected/newThis.columns) == Math.floor((selected + 1)/newThis.columns)
				&& selected + 1 < max)
			selected++

		if (nextProps.controls.directions.left && Math.floor(selected/newThis.columns) == Math.floor((selected - 1)/newThis.columns))
			selected--

		if (nextProps.controls.directions.down)
			if (selected + newThis.columns < max)
				selected += newThis.columns
			else
				selected = max - 1

		if (nextProps.controls.directions.up && selected - newThis.columns >= 0)
			selected -= newThis.columns


		// Scroll
		const elem = ReactDOM.findDOMNode(newThis.loginUsers.current).childNodes[0] as HTMLElement
		const child = elem.childNodes[selected] as HTMLElement
		child.scrollIntoView()	// TODO movimiento no queda bien, mejorar

		newThis.setState({ selected })
	}


	constructor(props: any) {
		super(props)

		this.state = {
			users: [],
			selected: -1,
		}

		this.repeater = new RepeaterDirection(150, 40, 8)
		this.loginUsers = React.createRef()

		this.openNewUserDialog = this.openNewUserDialog.bind(this)
		this.openLoginUserDialog = this.openLoginUserDialog.bind(this)
	}

	public componentDidMount() {
		userService.getUsers().then((users) => this.setState({ users }))

		this.calculateComuns()
	}

	private calculateComuns() {
		const elem = ReactDOM.findDOMNode(this.loginUsers.current).childNodes[0] as HTMLElement
		const child = elem.childNodes[0] as HTMLElement

		let width = elem.offsetWidth
		width -= parseFloat(window.getComputedStyle(elem).getPropertyValue('padding-left'))
		width -= parseFloat(window.getComputedStyle(elem).getPropertyValue('padding-right'))

		let widthChild = child.offsetWidth
		widthChild += parseFloat(window.getComputedStyle(child).getPropertyValue('margin-left'))
		widthChild += parseFloat(window.getComputedStyle(child).getPropertyValue('margin-right'))

		this.columns = Math.floor(width/widthChild)
	}

	public componentWillReceiveProps(nextProps: any) {
		if (nextProps.controls.directions && this.props.dialogClosed) {
			UsersComponentRaw.movePointer(this, nextProps)
			if (!nextProps.controls.directions.up && !nextProps.controls.directions.down
					&& !nextProps.controls.directions.left && !nextProps.controls.directions.right)
				this.repeater.stop()
			else
				this.repeater.start(() => {
					UsersComponentRaw.movePointer(this, nextProps)
				})
		}

		if (!this.props.dialogClosed)
			this.setState({ selected: -1 })

		if (nextProps.controls.resize)
			this.calculateComuns()

		if (nextProps.controls.buttons)
			if (nextProps.controls.buttons.enter) {
				let selected = document.querySelector('.login-user-selected') as HTMLElement
				if (selected && selected != null)
					selected.click()

				selected = document.querySelector('.login-new-user-selected') as HTMLElement
				if (selected && selected != null)
					selected.click()
			}
	}

	public openNewUserDialog() {
		store.dispatch(openDialog('new-user'))
	}

	public openLoginUserDialog(event: any) {
		store.dispatch(openDialog('login-user', { name: event.currentTarget.dataset.key }))
	}

	public render(): React.ReactNode {
		return (
			<div ref={this.loginUsers}>
				<CustomScroll className="login-users" top={ (Math.floor(this.state.selected/6.0)-1)*300 }>
					{
						this.state.users.map((user: User, i: number) => {
							return (
								<div className={'login-user ' + (this.state.selected == i ? 'login-user-selected' : '')} key={i} data-key={user.name}
										onClick={this.openLoginUserDialog}>
									<div>
										<img src={user.avatar.src} />
									</div>

									<div>{user.name}</div>
								</div>
							)
						})
					}
					<div className={'new-user ' + (this.state.selected == this.state.users.length ? 'login-new-user-selected' : '')}
						onClick={ this.openNewUserDialog }>
						<div>
							<i className="fas fa-user-plus"></i>
						</div>
					</div>
				</CustomScroll>
			</div>
		)
	}

}


const mapStateToProps = (state: any) => {
	return {
		controls: state.controls,
		resize: state.controls.resize,
		dialogClosed: state.dialog.closed,
	}
}

export const UsersComponent = connect(mapStateToProps)(UsersComponentRaw)
