import * as fs from 'fs'

export const imageService = new class {

	public IMAGE_MIME_TYPES: any

	constructor() {
		const canvas: HTMLCanvasElement = document.getElementById('hidden-canvas') as HTMLCanvasElement

		this.IMAGE_MIME_TYPES = []
		this.IMAGE_MIME_TYPES['bmp'] = 'image/bmp'
		this.IMAGE_MIME_TYPES['gif'] = 'image/gif'
		this.IMAGE_MIME_TYPES['jpe'] = 'image/jpe'
		this.IMAGE_MIME_TYPES['jpeg'] = 'image/jpeg'
		this.IMAGE_MIME_TYPES['jpg'] = 'image/jpg'
		this.IMAGE_MIME_TYPES['svg'] = 'image/svg+xml'
		this.IMAGE_MIME_TYPES['tiff'] = 'image/tiff'
		this.IMAGE_MIME_TYPES['ico'] = 'image/x-icon'
		this.IMAGE_MIME_TYPES['png'] = 'image/png'
		this.IMAGE_MIME_TYPES['wbmp'] = 'image/vnd.wap.wbmp'
	}


	public getImageFromFile(path: string, defaultURL: string): Promise<HTMLImageElement> {
		return new Promise((resolve, reject) => {
			fs.readFile(path, (err, data) => {
				if (err) {
					reject(err)
					return
				}

				const buffer = new ArrayBuffer(data.length)
				const bytes = new Uint8Array(buffer)
				for (let i = 0; i < data.length; i++)
					bytes[i] = data.readInt8(i)

				const img = new Image()

				const index = path.lastIndexOf('.')
				if (index < 0 || index == path.length)
					img.src = defaultURL
				else {
					const ext = path.substring(index + 1)
					img.src = URL.createObjectURL(new Blob([bytes], { type: this.IMAGE_MIME_TYPES[ext] }))
				}

				img.onload = () => resolve(img)
				img.onerror = reject
			})
		})
	}

	public getImageFromURL(url: string): Promise<HTMLImageElement> {
		return new Promise((resolve, reject) => {
			const img = new Image()

			img.src = url

			img.onload = () => resolve(img)
			img.onerror = reject
		})
	}

	public transformImage(image: HTMLImageElement, mimeResult: string, callback: (pixel: any[]) => any[]): Promise<string> {
		return new Promise((resolve) => {
			const canvas = document.createElement('canvas')
			const ctx: CanvasRenderingContext2D = canvas.getContext('2d')
			canvas.width = image.width
			canvas.height = image.height
			ctx.drawImage(image, 0, 0)

			const data = ctx.getImageData(0, 0, image.width, image.height)

			for (let i = 0; i < data.data.length; i += 4) {
				const pixel = [data.data[i], data.data[i + 1], data.data[i + 2], data.data[i + 3]]

				const newPixel = callback(pixel)
				data.data[i] = newPixel[0]
				data.data[i + 1] = newPixel[1]
				data.data[i + 2] = newPixel[2]
				data.data[i + 3] = newPixel[3]
			}

			ctx.putImageData(data, 0, 0)

			resolve(canvas.toDataURL(mimeResult))
		})
	}

	public hslToRgb(h: number, s: number, l: number) {
		let r
		let g
		let b

		if (s == 0)
			r = g = b = l // achromatic
		else {
			const hue2rgb = (p: number, q: number, t: number) => {
				if (t < 0) t += 1
				if (t > 1) t -= 1
				if (t < 1 / 6) return p + (q - p) * 6 * t
				if (t < 1 / 2) return q
				if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6
				return p
			}

			const q2 = l < 0.5 ? l * (s + 1) : l + s - l * s
			const p2 = l * 2 - q2
			r = hue2rgb(p2, q2, h + 1 / 3)
			g = hue2rgb(p2, q2, h)
			b = hue2rgb(p2, q2, h - 1 / 3)
		}

		return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)]
	}

}()
