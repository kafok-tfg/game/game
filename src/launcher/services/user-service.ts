import * as crypto from 'crypto'
import * as electron from 'electron'
import * as fs from 'fs'
import { User } from '../models/users'
import { imageService } from './image-service'

const app = electron.remote.app

export const userService = new class {

	public getUsers(): Promise<User[]> {
		return new Promise((resolve) => {
			fs.readdir(app.getPath('userData') + '/launcher/users', async (err, files) => {
				if (err) {
					resolve([])
					return
				}

				const users = []
				for (const file of files) {
					if (!file.endsWith('.data'))
						continue

					const user: User = new User()
					user.name = file.substring(0, file.length - 5)

					try {
						await fs.promises.access(app.getPath('userData') + '/launcher/users/' + user.name + '.jpg', fs.constants.F_OK)
						user.avatar = await imageService.getImageFromFile(app.getPath('userData') + '/launcher/users/' + user.name + '.jpg', '')
					} catch (error) {
						const image = new Image()
						image.src = '/assets/default-avatar.png'
						await new Promise((res) => {
							const color = Math.random()

							image.onload = async () => {
								image.src = await imageService.transformImage(image, 'image/jpeg', (pixel) => {
									const newColor = imageService.hslToRgb(color, 1, pixel[0] == 0 ? 0.8 : 0.5)
									return [newColor[0], newColor[1], newColor[2], 255]
								})
								user.avatar = image
								image.onload = null

								fs.writeFile(app.getPath('userData') + '/launcher/users/' + user.name + '.jpg',
									Buffer.from(image.src.substring(23), 'base64'),
									(error) => {
										if (error)
											return
									},
								)

								res()
							}
						})
					}

					users.push(user)
				}

				resolve(users)
			})
		})
	}

	public createUser(user: User): Promise<any> {
		return new Promise(async (resolve, reject) => {
			const fileName = app.getPath('userData') + '/launcher/users/' + user.name + '.data'

			await fs.promises.mkdir(app.getPath('userData') + '/launcher/users', { recursive: true })
			let exists = false
			try {
				await fs.promises.stat(fileName)
				exists = true
			} catch (err) {
				exists = false
			}

			if (exists) {
				reject('exists')
				return
			}

			fs.writeFile(fileName, JSON.stringify({
				name: user.name,
				password: crypto.createHash('sha256').update(user.password).digest('hex'),
			}), 'utf-8', (err) => {
				if (err)
					reject(err)

				resolve()
			})
		})
	}

	public async setAvatar(name: string, avatar: string): Promise<void> {

		let dataBase64 = (await imageService.transformImage(
			await imageService.getImageFromURL(avatar),
			'image/jpeg',
			(pixel) => {
				return pixel
			}))

		dataBase64 = dataBase64.substring(dataBase64.indexOf('base64,') + 7)

		return fs.promises.writeFile(app.getPath('userData') + '/launcher/users/' + name + '.jpg', Buffer.from(dataBase64, 'base64'))
	}

	public loginUser(name: string, password: string): Promise<any> {
		return new Promise(async (resolve, reject) => {
			const fileName = app.getPath('userData') + '/launcher/users/' + name + '.data'

			let user = null
			try {
				user = JSON.parse(await fs.promises.readFile(fileName, 'utf-8'))
			} catch (err) {
				user = null
			}

			if (user == null) {
				reject('exists')
				return
			}

			if (user.password == crypto.createHash('sha256').update(password).digest('hex'))
				resolve()
			else
				reject('bad-password')

		})
	}

}()
