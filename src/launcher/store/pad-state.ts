import * as launcher from 'launcher-api'

// Types

export interface PadState {
	close: boolean,
	fullscreen: boolean,
}

export enum PadStateTypes {
	ADD_BUTTON = 'ADD_BUTTON',
	REMOVE_BUTTON = 'REMOVE_BUTTON',
}

export interface PadStateAction {
	type: PadStateTypes,
	state: string
}


// Actions

export function addButton(name: string): PadStateAction {
	return {
		type: PadStateTypes.ADD_BUTTON,
		state: name,
	}
}

export function removeButton(name: string): PadStateAction {
	return {
		type: PadStateTypes.REMOVE_BUTTON,
		state: name,
	}
}


// Reducers

const initialState: PadState = {
	close: false,
	fullscreen: true,
}

export function PadStateReducer(state: PadState = initialState, action: PadStateAction): PadState {
	let res: any = null
	switch (action.type) {
		case PadStateTypes.ADD_BUTTON:
			launcher.virtualPadAPI.sendDataString(null, 500, '+' + action.state)
			res = {...state}
			res[action.state] = true
			return res

		case PadStateTypes.REMOVE_BUTTON:
			launcher.virtualPadAPI.sendDataString(null, 500, '-' + action.state)
			res = {...state}
			res[action.state] = true
			return res

		default:
			return state
	}
}
