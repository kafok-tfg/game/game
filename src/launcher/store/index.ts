import { combineReducers, createStore } from 'redux'
import { ControlsReducer } from './controls'
import { DialogReducer } from './dialog'
import { PadStateReducer } from './pad-state'

export const rootReducer = combineReducers({
	dialog: DialogReducer,
	controls: ControlsReducer,
	padState: PadStateReducer,
})

export const store = createStore(rootReducer)
