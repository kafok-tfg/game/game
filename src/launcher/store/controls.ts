
// Types

export interface ControlsState {
	directions: {
		up: boolean,
		down: boolean,
		left: boolean,
		right: boolean,
	},
	buttons: {
		enter: boolean,
		back: boolean,
		close: boolean,
	}
	resize: {
		width: number,
		height: number,
	}
}

export enum ControlsActionTypes {
	CHANGE_DIRECTIONS = 'CHANGE_DIRECTIONS',
	CHANGE_BUTTONS = 'CHANGE_BUTTONS',
	RESIZE = 'RESIZE',
}

export interface ControlsAction {
	type: ControlsActionTypes,
	state: ControlsState
}


// Actions

export function changeDirections(directions: { up: boolean, down: boolean, left: boolean, right: boolean }): ControlsAction {
	return {
		type: ControlsActionTypes.CHANGE_DIRECTIONS,
		state: {
			directions,
			buttons: undefined,
			resize: undefined,
		},
	}
}

export function changeButtons(buttons: { enter: boolean, back: boolean, close: boolean }): ControlsAction {
	return {
		type: ControlsActionTypes.CHANGE_BUTTONS,
		state: {
			directions: undefined,
			buttons,
			resize: undefined,
		},
	}
}

export function resize(sizes: { width: number, height: number }): ControlsAction {
	return {
		type: ControlsActionTypes.RESIZE,
		state: {
			directions: undefined,
			buttons: undefined,
			resize: sizes,
		},
	}
}

// Reducers

const initialState: ControlsState = {
	directions: { up: false, down: false, left: false, right: false },
	buttons: { enter: false, back: false, close: false },
	resize: { width: -1, height: -1 },
}

export function ControlsReducer(state: ControlsState = initialState, action: ControlsAction): ControlsState {
	switch (action.type) {
		case ControlsActionTypes.CHANGE_DIRECTIONS:
			return { directions: action.state.directions, buttons: state.buttons, resize: state.resize }

		case ControlsActionTypes.CHANGE_BUTTONS:
			return { directions: state.directions, buttons: action.state.buttons, resize: state.resize }

		case ControlsActionTypes.RESIZE:
			return { directions: state.directions, buttons: state.buttons, resize: action.state.resize }

		default:
			return state
	}
}
