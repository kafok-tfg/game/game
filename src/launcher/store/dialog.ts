import { store } from './index'
import { addButton, removeButton } from './pad-state'

// Types

export interface DialogState {
	dialogs: any
	data: any
}

export enum DialogActionTypes {
	TOGGLE_DIALOG = 'TOGGLE_DIALOG',
}

export interface DialogAction {
	type: DialogActionTypes,
	state: {
		closed: boolean,
		name: string,
		data: any,
	}
}


// Actions

export function closeDialog(): DialogAction {
	store.dispatch(removeButton('close'))
	return {
		type: DialogActionTypes.TOGGLE_DIALOG,
		state: {
			closed: true,
			name: '',
			data: null,
		},
	}
}

export function openDialog(name: string = 'default', data: any = null): DialogAction {
	store.dispatch(addButton('close'))
	return {
		type: DialogActionTypes.TOGGLE_DIALOG,
		state: {
			closed: false,
			name,
			data,
		},
	}
}


// Reducers

const initialState: DialogState = {
	dialogs: {},
	data: null,
}

export function DialogReducer(state: DialogState = initialState, action: DialogAction): DialogState {
	let res = {...state}
	switch (action.type) {
		case DialogActionTypes.TOGGLE_DIALOG:
			if (action.state.closed)
				res = {
					dialogs: {},
					data: null,
				}
			else {
				res.dialogs[action.state.name] = true
				res.data = action.state.data
			}

			return res

		default:
			return state
	}
}
