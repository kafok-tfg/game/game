import { BufferUtils } from './../common/buffer-utils'
import { DataTypes } from './../common/constants/data-types'
import { RefusedErrors } from './../common/constants/refused-errors'
import { Sensors } from './../common/constants/sensors'
import { VirtualPadAPI } from './virtual-pad-api'

export const virtualPadAPI: VirtualPadAPI = new VirtualPadAPI()
export {
	BufferUtils,
	DataTypes,
	RefusedErrors,
	Sensors,
}
