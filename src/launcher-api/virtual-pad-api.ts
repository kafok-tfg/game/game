import { ipcRenderer } from 'electron'
import { DataTypes } from './../common/constants/data-types'

export class VirtualPadAPI {

	private readonly events: any
	private readonly eventsDefault: any
	private readonly eventsFile: any
	private devices: Array<{ id: number, address: string, tcpPort: number, udpPort: number }>
	private readonly converters: any
	private readonly callbacksFile: Map<string, (name: string) => Promise<{ name: string, mimeType: string, data: Buffer }>>

	private readonly sensorData: any


	constructor() {
		this.devices = new Array()
		this.events = []
		this.eventsFile = []
		this.sensorData = { }
		this.converters = { }
		this.callbacksFile = new Map()

		this.eventsDefault = []
		this.eventsDefault['connection-request'] = () => true

		ipcRenderer.on('virtual-pad-api-event', (event, message) => {
			this.fireEvent(message.type, message.data, message.workerId)
		})

		ipcRenderer.on('virtual-pad-api-data', (event, data: any) => {
			for (const device in data) {
				let old = this.sensorData[device]
				if (old == undefined)
					old = { }

				this.sensorData[device] = Object.assign(old, data[device])
			}
		})

		ipcRenderer.on('virtual-pad-api-connect', (event, device) => {
			this.devices.push(device)
		})

		ipcRenderer.on('virtual-pad-api-disconnect', (event, device) => {
			this.devices = this.devices.filter((value) => value.address != device.address)
		})

		ipcRenderer.on('virtual-pad-api-file-callback', (event, params) => {
			const callback = this.callbacksFile.get(params.path)
			if (callback)
				callback(params.name)
					.then((res) => ipcRenderer.send('virtual-pad-api-file-callback-response', res))
					.catch(() => ipcRenderer.send('virtual-pad-api-file-callback-response',
						{ name: params.name, mimeType: undefined, data: Buffer.alloc(0) }))

		})

		ipcRenderer.send('virtual-pad-api-loaded', { })
	}


	public setName(name: string) {
		ipcRenderer.send('virtual-pad-api-name', { name })
	}


	public onConnectionRequest(listener:
		(params: { sensors: number[], address?: string, port?: number, timeout?: number }) => Promise<boolean | number[]> | boolean | number[]) {
		this.registerEvent('connection-request', listener)
	}

	public onConnect(listener: (params: { address: string, tcpPort: number, udpPort: number }) => void) {
		this.registerEvent('connect', listener)
	}

	public onDisconnect(listener: (params: { address: string, port: number }) => void) {
		this.registerEvent('disconnect', listener)
	}

	public onReceiveFile(listener: (params: { name: string, mimeType: string, file: Buffer }) => void) {
		this.registerEvent('send-file', (params: any) => {
			params.file = Buffer.from(params.file, 'hex')
			listener(params)

			const event = this.eventsFile[name]
			if (event)
				event(params)
		})
	}

	public onReceiveFileName(file: string, listener: (params: { name: string, mimeType: string, file: Buffer }) => void) {
		if (listener == undefined)
			delete this.eventsFile[name]
		else
			this.eventsFile[name] = listener
	}


	public addFile(name: string, path: string, mimeType: string) {
		ipcRenderer.send('virtual-pad-api-add-file', { name, config: { type: 'file', path, mimeType } })
	}

	public addFileUrl(name: string, path: string, base?: string) {
		ipcRenderer.send('virtual-pad-api-add-file', { name, config: { type: 'url', path, base } })
	}

	public addFileFolder(path: string) {
		ipcRenderer.send('virtual-pad-api-add-file', { config: { type: 'folder', path } })
	}

	public addFileUrlProxy(base: string, path: string) {
		ipcRenderer.send('virtual-pad-api-add-file', { name, config: { type: 'url-proxy', base, path } })
	}

	public addFileCallback(path: string, callback: (name: string) => Promise<{ name: string, mimeType: string, data: Buffer }>) {
		ipcRenderer.send('virtual-pad-api-add-file', { name: path, config: { type: 'callback', path } })
		this.callbacksFile.set(path, callback)
	}

	public removeFile(name: string) {
		ipcRenderer.send('virtual-pad-api-remove-file', name)
	}

	public removeFileFolder(name: string) {
		ipcRenderer.send('virtual-pad-api-remove-file-folder', name)
	}

	public removeFileUrlProxy(name: string) {
		ipcRenderer.send('virtual-pad-api-remove-file-proxy', name)
	}

	public removeFileCallback(name: string) {
		ipcRenderer.send('virtual-pad-api-remove-file-callback', name)
	}


	public setDataFrecuency(frecuency: number) {
		ipcRenderer.send('virtual-pad-api-data-frecuency', frecuency)
	}

	public setConverter(idSensor: number, converter: { dataType: string,  length: number }) {
		ipcRenderer.send('virtual-pad-api-set-converter', { add: true, converter, idSensor })
		this.converters['' + idSensor] = converter
	}

	public removeConverter(idSensor: number) {
		ipcRenderer.send('virtual-pad-api-set-converter', { add: false, converter: null, idSensor })
		delete this.converters[idSensor]
	}


	public getDevices(): Array<{ id: number, address: string, tcpPort: number, udpPort: number }> {
		return this.devices
	}

	public getData(idSensor: number, device: string): any[] {
		const converter = this.converters['' + idSensor]
		if (!converter)
			return undefined

		const defaultValue: any[] = DataTypes.getDefaultValues(converter.dataType, converter.length)

		let res = this.sensorData[device]
		if (res) {
			res = res[idSensor]
			return res == undefined ? defaultValue : res
		}

		return defaultValue
	}

	public sendDataBoolean(device: string, idSensor: number, data: boolean[]) {
		const buffer = Buffer.alloc(data.length*4)

		let i = 0
		for (const dat of data) {
			buffer.writeInt32BE(dat ? 1 : 0, i*4)
			i++
		}

		if (device == null)
			for (const d of this.devices)
				ipcRenderer.send('virtual-pad-api-data', { device: d.address, idSensor, data: buffer.toString('hex')})
		else
			ipcRenderer.send('virtual-pad-api-data', { device, idSensor, data: buffer.toString('hex')})
	}

	public sendDataInt(device: string, idSensor: number, data: number[]) {
		const buffer = Buffer.alloc(data.length*4)

		let i = 0
		for (const dat of data) {
			buffer.writeInt32BE(dat, i*4)
			i++
		}

		if (device == null)
			for (const d of this.devices)
				ipcRenderer.send('virtual-pad-api-data', { device: d.address, idSensor, data: buffer.toString('hex')})
		else
			ipcRenderer.send('virtual-pad-api-data', { device, idSensor, data: buffer.toString('hex')})
	}

	public sendDataFloat(device: string, idSensor: number, data: number[]) {
		const buffer = Buffer.alloc(data.length*4)

		let i = 0
		for (const dat of data) {
			buffer.writeFloatBE(dat, i*4)
			i++
		}

		if (device == null)
			for (const d of this.devices)
				ipcRenderer.send('virtual-pad-api-data', { device: d.address, idSensor, data: buffer.toString('hex')})
		else
			ipcRenderer.send('virtual-pad-api-data', { device, idSensor, data: buffer.toString('hex')})
	}

	public sendDataString(device: string, idSensor: number, data: string) {
		if (device == null)
			for (const d of this.devices)
				ipcRenderer.send('virtual-pad-api-data', { device: d.address, idSensor, data: Buffer.from(data, 'utf-8').toString('hex')})
		else
			ipcRenderer.send('virtual-pad-api-data', { device, idSensor, data: Buffer.from(data, 'utf-8').toString('hex')})
	}

	public sendFile(device: string, name: string, mimeType: string, data: Buffer) {
		ipcRenderer.send('virtual-pad-api-send-file', { device, name, mimeType, data: data.toString('hex')})
	}


	private registerEvent(event: string, listener: any) {
		if (listener == undefined)
			delete this.events[name]
		else
			this.events[event] = listener
	}

	private fireEvent(type: string, data: any, workerId: number) {
		const defaultEvent = this.eventsDefault[type]

		// All events registered
		let listener: any
		const events = this.events[type]
		if (events != undefined)
			listener = events(data)

		// Default event
		if ((events == undefined || events.length == 0) && defaultEvent != undefined)
			listener = defaultEvent(data)

		// Return
		if (listener != undefined && listener != null && workerId != undefined && workerId != null)
			if (listener.then)
				listener.then((result: any) => this.responseEvent(type, result, workerId))
			else
				this.responseEvent(type, listener, workerId)
	}

	private responseEvent(type: string, result: any, workerId: number) {
		ipcRenderer.send('virtual-pad-api-event', { type, result, workerId })
	}
}
