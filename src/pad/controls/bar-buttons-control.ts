import './bar-buttons-control-styles'
import { IControl } from './icontrol'


export class BarButtonsControl extends IControl {

	private readonly html: string
	private readonly sensorId: number


	constructor(html: string, sensorId: number) {
		super()
		this.html = html
		this.sensorId = sensorId
	}


	public init(): void {
		document.getElementById(this.id).addEventListener('touchstart', () => API.sendDataBoolean(this.sensorId, [true]))
		document.getElementById(this.id).addEventListener('touchend', () => API.sendDataBoolean(this.sensorId, [false]))
	}

	public render(): string {
		return `
			<div class="bar-buttons-control" id="${this.id}">
				${this.html}
			</div>
		`
	}

}
