import './arrows-control-styles'
import { IControl } from './icontrol'

export class ArrowsControl extends IControl {

	private readonly sensorId: number


	constructor(sensorId: number) {
		super()
		this.sensorId = sensorId
	}


	public init(): void {
		const elem = document.getElementById(this.id)
		const event = (ev: TouchEvent) => {

			const rect = elem.getBoundingClientRect()
			for (const touch of ev.targetTouches) {
				const pointX = (touch.clientX - rect.left)/elem.clientWidth*1.0
				const pointY = (touch.clientY - rect.top)/elem.clientHeight*1.0

				const res = [false, false, false, false]
				if (pointY <= 0.33) res[0] = true
				if (pointY > 0.66) res[1] = true
				if (pointX <= 0.33) res[2] = true
				if (pointX > 0.66) res[3] = true

				API.sendDataBoolean(this.sensorId, res)
			}
		}

		elem.addEventListener('touchstart', event)
		elem.addEventListener('touchmove', event)
		elem.addEventListener('touchend', () => API.sendDataBoolean(this.sensorId, [false, false, false, false]))
	}

	public render(): string {
		return `
			<div class="arrows-control" id="${this.id}">
				<div class="up"></div>
				<div class="right"></div>
				<div class="bottom"></div>
				<div class="left"></div>
				<div class="center"></div>
			</div>
		`
	}

}
