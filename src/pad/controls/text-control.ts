import './../api'
import { IControl } from './icontrol'


export class TextControl extends IControl {

	constructor() {
		super()
	}

	public init(): void {
		document.getElementById(this.id).addEventListener('focus', () => API.unlock())
		document.getElementById(this.id).addEventListener('blur', () => API.lock())
	}

	public render(): string {
		return `
			<input type="text" id="${this.id}">
		`
	}

}
