
export abstract class IControl {

	private static idCounter: number = 1

	protected id: string


	constructor() {
		this.id = '_' + IControl.idCounter
		IControl.idCounter++
	}


	public abstract init(): void
	public abstract render(): string

}
