import './../api'
import './action-buttons-control-styles'
import { IControl } from './icontrol'


export class ActionButtonsControl extends IControl {

	private readonly actionHtml: string
	private readonly backHtml: string
	private readonly actionSensorId: number
	private readonly backSensorId: number

	constructor(actionHtml: string, backHtml: string, actionSensorId: number, backSensorId: number) {
		super()
		this.actionHtml = actionHtml
		this.backHtml = backHtml
		this.actionSensorId = actionSensorId
		this.backSensorId = backSensorId
	}

	public init(): void {
		document.getElementById(this.id + '_back').addEventListener('touchend', () => {
			API.sendDataBoolean(this.backSensorId, [true])
			setInterval(() => API.sendDataBoolean(this.backSensorId, [false]), 80)
		})

		document.getElementById(this.id + '_action').addEventListener('touchend', () => {
			API.sendDataBoolean(this.actionSensorId, [true])
			setInterval(() => API.sendDataBoolean(this.actionSensorId, [false]), 80)
		})
	}

	public render(): string {
		return `
			<div class="action-buttons-control">
				<div class="back" id="${this.id + '_back'}">${this.backHtml}</div>
				<div class="action" id="${this.id + '_action'}">${this.actionHtml}</div>
			</div>
		`
	}

}
