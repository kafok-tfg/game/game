import './api'
import { barButtonsState, setBarButtonsState } from './bar-buttons-state'
import { ActionButtonsControl } from './controls/action-buttons-control'
import { ArrowsControl } from './controls/arrows-control'
import './global'
import { icons } from './icons'
import { layoutManager } from './layout-manager'

API.lock()

layoutManager.addLayout('standard', `
	<div class="standard-layout">
		<div class="header"><div id="1"></div></div>
		<div id="2" class="middle-right"></div>
		<div id="3" class="middle-center"></div>
		<div id="4" class="middle-left"></div>
		<div id="5" class="arrows"></div>
		<div id="6" class="footer"></div>
		<div id="7" class="action-buttons"></div>
	</div>
`)


// State

API.requestFile('pad-state', (name, mimeType, data) => {
	const res = JSON.parse(API.util.arrayToString(data))
	for (const prop in res)
		barButtonsState[prop] = res[prop]

	setBarButtonsState('standard')
})

API.onDataSensor(500, (idSensor, data) => {
	const action = API.util.arrayToString(data)
	barButtonsState[action.substring(1)] = action.substring(0, 1) == '+'

	layoutManager.removeControls('standard', '1')
	setBarButtonsState('standard')
})


// Render

layoutManager.setControl('standard', '5', new ArrowsControl(102))
layoutManager.setControl('standard', '7', new ActionButtonsControl(
	icons['location-arrow-solid'],
	icons['chevron-left-solid'],
	100, 101))

layoutManager.render('standard')
