import { BarButtonsControl } from './controls/bar-buttons-control'
import { icons } from './icons'
import { layoutManager } from './layout-manager'

export const barButtonsState: any = []
export const setBarButtonsState = (layout: string) => {
	if (barButtonsState['close'])
		layoutManager.addControl(layout, '1', new BarButtonsControl(icons['times-solid'], 105))

	if (barButtonsState['fullscreen'])
		layoutManager.addControl(layout, '1', new BarButtonsControl(icons['expand-solid'], 103))
	else
		layoutManager.addControl(layout, '1', new BarButtonsControl(icons['compress-solid'], 104))
}
