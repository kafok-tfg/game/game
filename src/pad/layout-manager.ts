import * as $ from 'jquery'
import { IControl } from './controls/icontrol'
import './layouts'

class LayoutManager {

	private readonly layouts: any
	private readonly controls: any
	private activeLayout: string


	constructor() {
		this.layouts = []
		this.controls = []
		this.activeLayout = null
	}


	public addLayout(name: string, html: string) {
		this.layouts[name] = html
		this.controls[name] = []
	}

	public setControl(layout: string, id: string, control: IControl) {
		this.controls[layout][id] = control

		if (this.activeLayout != null)
			this.partialRender(layout, id)
	}

	public addControl(layout: string, id: string, control: IControl) {
		if (!this.controls[layout][id] || !this.controls[layout][id].length)
			this.controls[layout][id] = []

		this.controls[layout][id].push(control)

		if (this.activeLayout != null) {
			$('#' + id).html('')
			this.partialRender(layout, id)
		}
	}

	public removeControls(layout: string, id: string) {
		delete this.controls[layout][id]

		if (this.activeLayout != null)
			this.partialRender(layout, id)
	}

	public removeAllControls(layout: string) {
		delete this.controls[layout]
	}


	public render(layout: string) {
		this.activeLayout = layout

		const html = this.layouts[layout]
		const root = $('#root')
		root.html('')
		if (html) {
			root.html(html)

			for (const controlId in this.controls[layout])
				this.partialRender(layout, controlId)
		}
	}

	private partialRender(layout: string, id: string) {
		const control = this.controls[layout][id]

		if (control)
			if (control.length)
				for (const c of control) {
					$('#' + id).append(c.render())
					c.init()
				}
			else {
				$('#' + id).html(control.render())
				control.init()
			}
		else
			$('#' + id).html('')
	}

}

export const layoutManager = new LayoutManager()
