import { BufferUtils } from './../../common/buffer-utils'
import { RefusedErrors } from './../../common/constants/refused-errors'
import { Sensors } from './../../common/constants/sensors'

export class Message {

	// Constants

	public static readonly SCAN: number 							= 0x01
	public static readonly ECHO: number 							= 0x02
	public static readonly CONNECTION_REQUEST: number 				= 0x03
	public static readonly CONNECTION_SUCCESSFUL_RESPONSE: number 	= 0x04
	public static readonly CONNECTION_REFUSED_RESPONSE: number 		= 0x05
	public static readonly HEARTBEAT: number 						= 0x06
	public static readonly DATA: number 							= 0x07
	public static readonly REQUEST_FILE: number 					= 0x08
	public static readonly RESPONSE_FILE: number 					= 0x09
	public static readonly REQUEST_SET_CONTROLER: number 			= 0x0A
	public static readonly SEND_FILE: number 						= 0x0B


	// Attributes

	public type: number
	public size: number
	public data: Buffer


	// Create packet

	public static createEcho(name: Buffer, tcpPort: number, udpPort: number, timestampA: number, timestampB: number): Buffer {
		const res = Buffer.alloc(name.length + 28)
		res.writeInt32BE(Message.ECHO, 0)
		res.writeInt32BE(res.length, 4)
		res.writeInt32BE(udpPort, 8)
		res.writeInt32BE(tcpPort, 12)
		res.writeInt32BE(name.length, 16)
		BufferUtils.copyBuffer(res, name, 20)
		res.writeInt32BE(timestampA, name.length + 20)
		res.writeInt32BE(timestampB, name.length + 24)

		return res
	}

	public static createConnectionSuccessfulResponse(timeout: number): Buffer {
		const res = Buffer.alloc(12)
		res.writeInt32BE(Message.CONNECTION_SUCCESSFUL_RESPONSE, 0)
		res.writeInt32BE(res.length, 4)
		res.writeInt32BE(timeout, 8)

		return res
	}

	public static createConnectionRefuseResponse(type: number, sensors: number[]): Buffer {
		let res: Buffer = null
		if (type == RefusedErrors.LACK_OF_SENSORS)
			res = Buffer.alloc(sensors.length*4 + 16)
		else if (type == RefusedErrors.ENOUGH_DEVICES)
			res = Buffer.alloc(12)

		res.writeInt32BE(Message.CONNECTION_REFUSED_RESPONSE, 0)
		res.writeInt32BE(res.length, 4)
		res.writeInt32BE(type, 8)

		if (type == RefusedErrors.LACK_OF_SENSORS) {
			res.writeInt32BE(sensors.length, 12)

			let i = 0
			for (const sensor of sensors) {
				res.writeInt32BE(sensor, i*4 + 16)
				i++
			}
		}

		return res
	}

	public static createHeartbeat(): Buffer {
		const res = Buffer.alloc(12)
		res.writeInt32BE(Message.HEARTBEAT, 0)
		res.writeInt32BE(res.length, 4)

		return res
	}

	public static createRequestSetController(): Buffer {
		const res = Buffer.alloc(8)
		res.writeInt32BE(Message.REQUEST_SET_CONTROLER, 0)
		res.writeInt32BE(res.length, 4)

		return res
	}

	public static createData(idSensor: number, data: Buffer): Buffer {
		const res = Buffer.alloc(data.length + 12)
		res.writeInt32BE(Message.DATA, 0)
		res.writeInt32BE(res.length, 4)
		res.writeInt32BE(idSensor, 8)
		BufferUtils.copyBuffer(res, data, 12)

		return res
	}

	public static createDataEchoReq(size: number, data: Buffer): Buffer {
		const res = Buffer.alloc(size)
		res.writeInt32BE(Message.DATA, 0)
		res.writeInt32BE(res.length, 4)
		res.writeInt32BE(Sensors.ECHO_RES, 8)
		BufferUtils.copyBuffer(res, data, 12, size - 12)

		return res
	}

	public static createSendFile(name: string, mimeType: string, data: Buffer): Buffer {
		const nameBuffer = Buffer.from(name, 'utf-8')
		const mimeTypeBuffer = Buffer.from(mimeType, 'utf-8')

		const res = Buffer.alloc(nameBuffer.length + mimeType.length + data.length + 20)
		res.writeInt32BE(Message.SEND_FILE, 0)
		res.writeInt32BE(res.length, 4)
		res.writeInt32BE(nameBuffer.length, 8)
		BufferUtils.copyBuffer(res, nameBuffer, 12)
		res.writeInt32BE(mimeType.length, nameBuffer.length + 12)
		BufferUtils.copyBuffer(res, mimeTypeBuffer, nameBuffer.length + 16)
		res.writeInt32BE(data.length, nameBuffer.length + mimeType.length + 16)
		BufferUtils.copyBuffer(res, data, nameBuffer.length + mimeType.length + 20)

		return res
	}

	public static createResponseFile(name: string, mimeType: string, data: Buffer): Buffer {
		if (data == null)
			return

		if (!mimeType)
			mimeType = 'application/octet-stream'

		const nameBuffer = Buffer.from(name, 'utf-8')
		const mimeTypeBuffer = Buffer.from(mimeType, 'utf-8')

		const res = Buffer.alloc(nameBuffer.length + mimeType.length + data.length + 20)
		res.writeInt32BE(Message.RESPONSE_FILE, 0)
		res.writeInt32BE(res.length, 4)
		res.writeInt32BE(nameBuffer.length, 8)
		BufferUtils.copyBuffer(res, nameBuffer, 12)
		res.writeInt32BE(mimeType.length, nameBuffer.length + 12)
		BufferUtils.copyBuffer(res, mimeTypeBuffer, nameBuffer.length + 16)
		res.writeInt32BE(data.length, nameBuffer.length + mimeType.length + 16)
		BufferUtils.copyBuffer(res, data, nameBuffer.length + mimeType.length + 20)

		return res
	}


	// Received packet

	public static receivedScan(packet: Buffer): { udpPort: number, timestampA: number, timestampB: number } {
		return {
			udpPort: packet.readInt32BE(8),
			timestampA: packet.readInt32BE(12),
			timestampB: packet.readInt32BE(16),
		}
	}

	public static receivedConnectionRequest(packet: Buffer): { timeout: number, udpPort: number, sensors: number[] } {
		const res: { timeout: number, udpPort: number, sensors: number[] } = {
			timeout: packet.readInt32BE(8),
			udpPort: packet.readInt32BE(12),
			sensors: [],
		}

		for (let i = 0; i<packet.readInt32BE(16); i++)
			res.sensors.push(packet.readInt32BE(i*4 + 16))

		return res
	}

	public static receivedRequestFile(packet: Buffer): string {
		const name = BufferUtils.copyBuffer(Buffer.alloc(packet.readInt32BE(8)), packet, 0, 12)
		return name.toString('utf-8')
	}

	public static receivedSendFile(packet: Buffer): { name: string, mimeType: string, data: Buffer } {
		let count = packet.readInt32BE(8)
		const name = BufferUtils.copyBuffer(Buffer.alloc(count), packet, 0, 12).toString('utf-8')
		let size = packet.readInt32BE(count + 12)
		const mimeType = BufferUtils.copyBuffer(Buffer.alloc(size), packet, 0, count + 16).toString('utf-8')
		count = count + size + 16
		size = packet.readInt32BE(count)
		const data = BufferUtils.copyBuffer(Buffer.alloc(size), packet, 0, count + 4)

		return { name, mimeType, data }
	}
}
