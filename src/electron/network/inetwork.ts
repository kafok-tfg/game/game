import { Socket } from 'net'


export interface INetwork {

	start(): void

	sendUDP(buffer: Buffer, host: string, port: number): void
	sendTCP(buffer: Buffer, socket: Socket, close: boolean, callback?: () => void): void

	close(): void

	getPortUDP(): number
	getPortTCP(): number
	isRunning(): boolean
}
