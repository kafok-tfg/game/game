import * as cluster from 'cluster'
import { Socket } from 'net'
import { BufferUtils } from '../../common/buffer-utils'
import { DataTypes } from '../../common/constants/data-types'
import { RefusedErrors } from '../../common/constants/refused-errors'
import { Sensors } from '../../common/constants/sensors'
import { INetwork } from './inetwork'
import { Message } from './message'
import { NetworkSocket } from './network-socket'

export class Protocol {

	public readonly network: INetwork

	private name: Buffer
	private readonly timeout: number
	private readonly instances: number

	private connections: Array<{ socket: Socket, udpPort: number }>
	private nextPort: number

	private readonly dataConverters: Map<number, { dataType: string, length: number }>
	private readonly converters: any


	constructor(timeout: number, instances: number) {
		this.network = new NetworkSocket(this)
		this.timeout = timeout
		this.instances = instances
		this.nextPort = 0
		this.connections = new Array()
		this.dataConverters = new Map()
		this.converters = []

		// Default behivor of data
		this.dataConverters.set(Sensors.ACCELEROMETER, 			{ dataType: 'float',  length: 3 })
		this.dataConverters.set(Sensors.GRAVITY, 				{ dataType: 'float',  length: 3 })
		this.dataConverters.set(Sensors.GYROSCOPE, 				{ dataType: 'float',  length: 3 })
		this.dataConverters.set(Sensors.LIGHT, 					{ dataType: 'float',  length: 1 })
		this.dataConverters.set(Sensors.LINEAR_ACCELERATION, 	{ dataType: 'float',  length: 3 })
		this.dataConverters.set(Sensors.MAGNETIC_FIELD, 		{ dataType: 'float',  length: 3 })
		this.dataConverters.set(Sensors.KEYBOARD, 				{ dataType: 'string', length: 1 })

		// From API
		cluster.worker.on('message', (msg) => {
			let data: Buffer = null
			if (msg.type != undefined)

				switch (msg.type) {

					case 'set-name':
						this.name = Buffer.from(msg.content.name, 'utf-8')
						for (const connection of this.connections) {
							this.network.sendUDP(
								Message.createEcho(this.name, this.network.getPortTCP(), this.network.getPortUDP(), 0, 0),
								connection.socket.remoteAddress, connection.udpPort)

							setTimeout(() => this.network.sendTCP(Message.createRequestSetController(), connection.socket, false), 1200)	// To prevent errors
						}

						if (!this.network.isRunning())
							this.network.start()
						break

					case 'request-set-controller':
						data = Message.createRequestSetController()
						for (const device of this.connections)
							this.network.sendTCP(data, device.socket, false)

						break

					case 'set-converter':
						this.dataConverters.set(msg.content.idSensor, msg.content.converter)
						break

					case 'remove-converter':
						this.dataConverters.delete(msg.content)
						break

					case 'data':
						data = Message.createData(msg.content.idSensor, Buffer.from(msg.content.data, 'hex'))

						if (msg.content.device == null)
							for (const connection of this.connections)
								this.network.sendUDP(data, connection.socket.remoteAddress, connection.udpPort)
						else
							for (const connection of this.connections)
								if (msg.content.device == connection.socket.remoteAddress) {
									this.network.sendUDP(data, connection.socket.remoteAddress, connection.udpPort)
									break
								}

						break

					case 'send-file':
						data = Message.createSendFile(msg.content.name, msg.content.mimeType, Buffer.from(msg.content.data, 'hex'))

						if (msg.content.device == null)
							for (const connection of this.connections)
								this.network.sendTCP(data, connection.socket, false)	// TODO callback de error, hay que notificar al usuario
						else
							for (const connection of this.connections)
								if (msg.content.device == connection.socket.remoteAddress) {
									this.network.sendTCP(data, connection.socket, false)	// TODO callback de error, hay que notificar al usuario
									break
								}

						break

				}
		})

		// Converters
		this.converters[DataTypes.typeBoolean] = (data: Buffer, conv: { dataType: string, length: number }): boolean[] => {
			const res = []
			for (let i = 0; i < conv.length; i++)
				res.push(data.readInt32BE(i * 4 + 12) == 1)

			return res
		}

		this.converters[DataTypes.typeInteger] = (data: Buffer, conv: { dataType: string, length: number }): number[] => {
			const res = []
			for (let i = 0; i < conv.length; i++)
				res.push(data.readInt32BE(i * 4 + 12))

			return res
		}

		this.converters[DataTypes.typeFloat] = (data: Buffer, conv: { dataType: string, length: number }): number[] => {
			const res = []
			for (let i = 0; i < conv.length; i++)
				res.push(data.readFloatBE(i * 4 + 12))

			return res
		}

		this.converters[DataTypes.typeString] = (data: Buffer, conv: { dataType: string, length: number }): string[] => {
			return [BufferUtils.copyBuffer(Buffer.alloc(data.readInt32BE(4) - 12), data, 0, 12).toString('utf-8')]
		}

		this.converters['byte'] = (data: Buffer): number[] => {
			const res = []
			for (let i = 0; i < data.readInt32BE(4) - 12; i++)
				res.push(data.readInt8(i + 12))

			return res
		}
	}

	public onMessage(message: Message, address: string, port: number, socket: Socket) {

		try {
			switch (message.type) {
				case Message.SCAN:
					this.onScan(message, address)
					break

				case Message.CONNECTION_REQUEST:
					this.onConnectionRequest(message, address, port, socket)
					break

				case Message.REQUEST_FILE:
					this.onRequestFile(message, address, port, socket)
					break

				case Message.DATA:
					this.onData(message, address, port, socket)
					break

				case Message.SEND_FILE:
					this.onSendFile(message, address, port, socket)
					break
			}
		} catch (error) {
			// tslint:disable-next-line: no-console
			console.log(error)
		}
	}


	private onScan(message: Message, address: string) {
		// Read received
		const received = Message.receivedScan(message.data)

		// Send
		const data = Message.createEcho(this.name, this.network.getPortTCP(), this.network.getPortUDP() + this.nextPort,
			received.timestampA, received.timestampB)
		this.network.sendUDP(data, address, received.udpPort)

		// Update the instance
		this.nextPort++
		if (this.nextPort == this.instances)
			this.nextPort = 0
	}

	private onConnectionRequest(message: Message, address: string, port: number, socket: Socket) {
		// Read received
		const received = Message.receivedConnectionRequest(message.data)

		process.send({ type: 'connection-request', content: { sensors: received.sensors, address, port, timeout: received.timeout } })
		cluster.worker.once('message', (msg) => {
			if (msg.type != undefined && msg.type == 'connection-request')
				if (msg.result === true) {	// Connection Successful Response

					// Send
					const data = Message.createConnectionSuccessfulResponse(this.timeout)

					this.network.sendTCP(data, socket, false, () => {

						// Connect
						socket.setKeepAlive(true)
						socket.setTimeout(this.timeout)
						const heartbeat = setInterval(() => {
							this.network.sendTCP(Message.createHeartbeat(), socket, false)
						}, Math.floor(received.timeout * 0.6))

						socket.on('close', () => {
							clearInterval(heartbeat)
							process.send({ type: 'disconnect', content: {
								address: socket.remoteAddress,
								port: socket.remotePort,
							}})
							this.connections = this.connections.filter((value) => value.socket.remoteAddress != socket.remoteAddress)
						})

						this.connections.push({ socket, udpPort: received.udpPort })
						process.send({ type: 'connect', content: {
							id: cluster.worker.id,
							address: socket.remoteAddress,
							tcpPort: socket.remotePort,
							udpPort: received.udpPort,
						}})

						// Send request-set-controller
						this.network.sendTCP(Message.createRequestSetController(), socket, false)

					})


				} else if (typeof msg.result == 'object') // Lack of sensors

					this.network.sendTCP(Message.createConnectionRefuseResponse(RefusedErrors.LACK_OF_SENSORS, msg.result), socket, true)

				else	// Enough devices

					this.network.sendTCP(Message.createConnectionRefuseResponse(RefusedErrors.ENOUGH_DEVICES, null), socket, true)
		})
	}

	private onRequestFile(message: Message, address: string, port: number, socket: Socket) {
		// Read received
		const file = Message.receivedRequestFile(message.data)

		process.send({ type: 'request-file', content: { file } })
		cluster.worker.once('message', (msg) => {
			if (msg.type != undefined && msg.type == 'request-file')
				if (msg.result)
					this.network.sendTCP(Message.createResponseFile(file, msg.result.mimeType, Buffer.from(msg.result.data, 'hex')), socket, false)
		})
	}

	private onData(message: Message, address: string, port: number, socket: Socket) {
		// Read received
		const idSensor = message.data.readInt32BE(8)

		if (idSensor == Sensors.ECHO_REQ)

			this.network.sendUDP(Message.createDataEchoReq(message.data.readInt32BE(4), message.data), address, port)

		else
			process.send({
				type: 'data',
				content: { idSensor, data: this.convert(idSensor, message.data), device: address },
			})
	}

	private onSendFile(message: Message, address: string, port: number, socket: Socket) {
		// Read received
		const receive = Message.receivedSendFile(message.data)

		process.send({ type: 'send-file', content: { name: receive.name, mimeType: receive.mimeType, file: receive.data.toString('hex') } })
	}

	private convert(idSensor: number, buffer: Buffer): any[] {
		const type = this.dataConverters.get(idSensor)
		return this.converters[!type ? 'byte' : type.dataType](buffer, type)
	}

}
