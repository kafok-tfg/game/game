import * as cluster from 'cluster'
import { createSocket, Socket } from 'dgram'
import { AddressInfo, createServer, Server, Socket as SocketTCP } from 'net'
import { BufferUtils } from '../../common/buffer-utils'
import { INetwork } from './inetwork'
import { Message } from './message'
import { Protocol } from './protocol'

export class NetworkSocket implements INetwork {

	// Attributes

	private readonly protocol: Protocol

	private socketUDP: Socket
	private socketTCP: Server

	private running: boolean


	// Constructor

	constructor(protocol: Protocol) {
		this.protocol = protocol
		this.running = false
	}


	// Methods

	public start(): void {
		this.running = true
		this.socketTCP = createServer((socket) => {

			let message: Message = null
			let count: number = 0

			socket.on('data', (data) => {
				if (message == null) {
					message = new Message()
					count = 0
					message.type = data.readInt32BE(0)
					message.size = data.readInt32BE(4)
					message.data =  Buffer.alloc(message.size)
				}

				BufferUtils.copyBuffer(message.data, data, count)
				count += data.length

				if (count >= message.size) {
					this.protocol.onMessage(message, socket.remoteAddress, socket.remotePort, socket)
					message = null
				}
			})

			socket.on('timeout', () => {
				// TODO cerrar conexion si existe
			})

			socket.on('close', () => {
				// TODO cerrar conexion si existe
			})

			socket.on('end', () => {
				// TODO cerrar conexion si existe
			})
		})
		this.socketUDP = createSocket('udp4')

		let portCount = 0
		let init: boolean = false
		const PORTS: number[] = [42000, 43000, 44000, 45000, 46000, 47000, 48000]
		const bind = () => {
			this.socketUDP.bind({ address: '0.0.0.0', port: PORTS[portCount] + cluster.worker.id - 1, exclusive: true })

			this.socketUDP.on('message', (data, remote) => {
				const message: Message = new Message()
				message.type = data.readInt32BE(0)
				message.size = data.readInt32BE(4)
				message.data = data

				this.protocol.onMessage(message, remote.address, remote.port, null)
			})

			this.socketUDP.on('listening', () => {
				this.socketUDP.setRecvBufferSize(262144) // 256kb
				init = true
				this.socketTCP.listen(PORTS[portCount] - 1, '0.0.0.0')
			})
		}

		bind()

		this.socketUDP.on('error', (err) => {
			if (!init) {
				this.socketUDP.close()
				this.socketUDP = createSocket('udp4')
				portCount++

				if (portCount >= PORTS.length)
					throw new Error('todo') // TODO
				else
					bind()
			}
		})

	}


	public sendUDP(buffer: Buffer, host: string, port: number): void {
		this.socketUDP.send(buffer, 0, buffer.length, port, host)
	}

	public sendTCP(buffer: Buffer, socket: SocketTCP, close: boolean, callback: () => void = null): void {
		socket.write(buffer, (err) => {
			if (close)
				socket.end()

			if (callback != null)
				callback()
		})
	}

	public close(): void {
		this.socketUDP.close()
		this.socketTCP.close()
	}


	public getPortUDP(): number {
		return (this.socketUDP.address() as AddressInfo).port
	}

	public getPortTCP(): number {
		return (this.socketTCP.address() as AddressInfo).port
	}

	public isRunning(): boolean {
		return this.running
	}
}
