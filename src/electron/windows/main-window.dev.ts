import { BrowserWindow, globalShortcut } from 'electron'
import { VirtualPadAPIBackend } from '../virtual-pad-api-backend'
import { WindowFactory } from './window-factory'

export class MainWindow extends WindowFactory {

	private win: BrowserWindow

	public create(virtualPadAPI: VirtualPadAPIBackend): BrowserWindow {
		// Create the browser window.
		this.win = new BrowserWindow({
			autoHideMenuBar: true,
			show: false,
			backgroundColor: '#ffffff',
			icon: `${this.base}/assets/icon.png`,
			webPreferences: {
				nodeIntegration: true,
			},
		})

		this.win.loadURL(`${this.base}`)

		// The DevTools.
		globalShortcut.register('Ctrl+F12', () => {
			this.win.webContents.openDevTools()
		})

		globalShortcut.register('F5', () => {
			this.win.loadURL(`${this.base}`)
		})

		// Event when the window is closed.
		this.win.on('closed', () => {
			this.win = null
			virtualPadAPI.setWindow(null)
		})

		this.win.once('ready-to-show', () => {
			this.win.maximize()
			this.win.removeMenu()
			this.win.show()
			virtualPadAPI.setWindow(this.win)
		})

		return this.win
	}

	public getWindow(): Electron.BrowserWindow {
		return this.win
	}


}
