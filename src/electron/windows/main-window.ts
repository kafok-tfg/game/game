import { BrowserWindow, globalShortcut } from 'electron'
import { VirtualPadAPIBackend } from '../virtual-pad-api-backend'
import { WindowFactory } from './window-factory'

export class MainWindow extends WindowFactory {

	private win: BrowserWindow

	public create(virtualPadAPI: VirtualPadAPIBackend): BrowserWindow {
		// Create the browser window.
		this.win = new BrowserWindow({
			autoHideMenuBar: true,
			show: false,
			backgroundColor: '#ffffff',
			icon: `${this.base}/assets/icon.png`,
			webPreferences: {
				devTools: false,
				nodeIntegration: true,
			},
		})

		this.win.loadURL(`${this.base}/index.html`)

		// Disallow devTools
		this.win.webContents.on('devtools-opened', () => {
			this.win.webContents.closeDevTools()
		})

		// Event when the window is closed.
		this.win.on('closed', () => {
			this.win = null
			virtualPadAPI.setWindow(null)
		})

		this.win.once('ready-to-show', () => {
			this.win.removeMenu()
			this.win.show()
			virtualPadAPI.setWindow(this.win)
		})

		return this.win
	}

	public getWindow(): Electron.BrowserWindow {
		return this.win
	}


}
