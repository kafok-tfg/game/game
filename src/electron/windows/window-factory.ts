import { BrowserWindow } from 'electron'
import { VirtualPadAPIBackend } from '../virtual-pad-api-backend'

export abstract class WindowFactory {

	protected base: string = __dirname

	public abstract create(virtualPadAPI: VirtualPadAPIBackend): BrowserWindow
	public abstract getWindow(): BrowserWindow

}
