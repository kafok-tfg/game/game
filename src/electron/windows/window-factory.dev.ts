import { BrowserWindow } from 'electron'
import { VirtualPadAPIBackend } from '../virtual-pad-api-backend'

export abstract class WindowFactory {

	protected base: string = 'http://localhost:8080'

	public abstract create(virtualPadAPI: VirtualPadAPIBackend): BrowserWindow
	public abstract getWindow(): BrowserWindow

}
