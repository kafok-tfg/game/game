import { app, BrowserWindow } from 'electron'
import { Enviroment } from './../common/enviroment'
import { VirtualPadAPIBackend } from './virtual-pad-api-backend'
import { MainWindow } from './windows/main-window'
import { WindowFactory } from './windows/window-factory'

export class Main {

	private win: BrowserWindow

	public createWindow(virtualPadAPI: VirtualPadAPIBackend) {
		const win: WindowFactory = new MainWindow()
		this.win = win.create(virtualPadAPI)
		if (!Enviroment.isProduction)
			this.win.webContents.on('did-start-loading', () => virtualPadAPI.sendMessageAll({
				type: 'request-set-controller',
			}))
	}

	public activate(virtualPadAPI: VirtualPadAPIBackend) {
		// macOS specific close process
		if (this.win === null)
			this.createWindow(virtualPadAPI)
	}

	public init(virtualPadAPI: VirtualPadAPIBackend) {
		// Create window on electron intialization
		if (app.isReady())
			this.createWindow(virtualPadAPI)
		else
			app.on('ready', () => this.createWindow(virtualPadAPI))

		// Quit when all windows are closed.
		app.on('window-all-closed', () => {

			// On macOS specific close process
			if (process.platform !== 'darwin')
				app.quit()

		})

		app.on('activate', () => this.activate(virtualPadAPI))
	}
}
