import * as cluster from 'cluster'
import { BrowserWindow, ipcMain } from 'electron'
import { readFile } from 'fs'
import * as http from 'http'
import * as https from 'https'
import { BufferUtils } from '../common/buffer-utils'
import { Enviroment } from '../common/enviroment'

export class VirtualPadAPIBackend {

	public readonly devices: Array<{ id: number, address: string, tcpPort: number, udpPort: number }>

	private readonly files: Map<string, any>
	private readonly filesFolder: Map<string, any>
	private readonly filesProxy: Map<string, any>
	private readonly callbacksFile: Map<string, (name: string) => { name: string, mimeType: string, data: Buffer }>
	private readonly callbacksResponses: Map<string, any>
	private win: BrowserWindow

	private sensorData: any
	private dataSender: any
	private dataFrecuency: number


	constructor() {
		this.files = new Map()
		this.filesFolder = new Map()
		this.filesProxy = new Map()
		this.devices = new Array()
		this.sensorData = { }
		this.dataFrecuency = 20
		this.callbacksFile = new Map()
		this.callbacksResponses = new Map()

		// Request Files event default
		const requestFileEvent = (params: any) => {
			let type: string = params.config.type
			if (params.name == 'controller' && !Enviroment.isProduction) {
				type = Enviroment.isProduction ? 'file' : 'url'
				params.config.base = 'http://localhost:8080/pad/'
			}

			params.config.base = params.config.base == undefined ? '' : params.config.base
			params.config.path = params.config.path == undefined ? '' : params.config.path

			const readUrl = (url: string) => {
				return new Promise((resolve) => {
					const httpCallback = (res: http.IncomingMessage) => {

						if (res.statusCode != 200) {
							resolve({ data: Buffer.alloc(0), mimeType: '' })
							return
						}

						let mimeType = res.headers['content-type']
						const index = mimeType.indexOf(';')
						if (index > 0)
							mimeType = mimeType.substring(0, index)

						const data: Buffer = Buffer.alloc(Number.parseInt(res.headers['content-length'], 10))
						let counter: number = 0
						res.on('data', (chunk) => {
							BufferUtils.copyBuffer(data, chunk, counter)
							counter += chunk.length
						})
						res.on('end', () => resolve({ data, mimeType }))

					}

					if (url.startsWith('https'))
						https.get(url, httpCallback).on('error', (err) => {
							// tslint:disable-next-line: no-console
							console.log(err)
							resolve({ data: Buffer.alloc(0), mimeType: '' })
						})
					else
						http.get(url, httpCallback).on('error', (err) => {
							// tslint:disable-next-line: no-console
							console.log(err)
							resolve({ data: Buffer.alloc(0), mimeType: '' })
						})
				})
			}

			switch (type) {
				case 'file':
					return new Promise((resolve) => {
						const path = Enviroment.isProduction ? `${__dirname}/${params.config.path}` : `./src${params.config.path}`
						readFile(path, (err, data) => {
							if (err)
								// tslint:disable-next-line: no-console
								console.log(err)

							if (!data)
								data = Buffer.alloc(0)

							resolve({ data, mimeType: params.config.mimeType })
						})
					})

				case 'url':
					return readUrl(params.config.base + params.config.path)

				case 'folder':
					return new Promise((resolve) => {
						const path = Enviroment.isProduction ?
							`${__dirname}/${params.config.path}/${params.name}` :
							`./src${params.name}`	// TODO esta ruta debe poderse usar en un juego tambien

						readFile(path, (err, data) => {
							if (err)
								// tslint:disable-next-line: no-console
								console.log(err)

							if (!data)
								data = Buffer.alloc(0)

							resolve({ data, mimeType: params.config.mimeType })
						})
					})

				case 'url-proxy':
					return readUrl(params.config.base + params.name.replace(params.name.path, ''))

				case 'callback':
					this.win.webContents.send('virtual-pad-api-file-callback', { name: params.name, path: params.config.path })
					return new Promise((resolve) => {
						this.callbacksResponses.set(params.name, resolve)
					})
			}
		}


		// Defaults events
		cluster.on('message', (worker: any, message: any) => {
			if (message.type != undefined)

				// Methods
				switch (message.type) {
					case 'connect':
						this.devices.push(message.content)
						this.win.webContents.send('virtual-pad-api-connect', message.content)
						this.fireEvent('connect', message.content, undefined)
						break

					case 'disconnect':
						this.devices.filter((value) => value.address != message.content.address)
						this.win.webContents.send('virtual-pad-api-disconnect', message.content)
						this.fireEvent('disconnect', message.content, undefined)
						break

					case 'request-file':
						let config = this.files.get(message.content.file)
						if (!config) {
							for (const values of this.filesFolder.entries())
								if (message.content.file.startsWith(values[0])) {
									config = values[1]
									break
								}

							if (!config) {
								for (const values of this.filesProxy.entries())
									if (message.content.file.startsWith(values[0])) {
										config = values[1]
										break
									}

								if (!config)
									for (const values of this.callbacksFile.entries())
										if (message.content.file.startsWith(values[0])) {
											config = values[1]
											break
										}
							}
						}

						const file = config ?
							requestFileEvent({ name: message.content.file, config }) :
							new Promise((resolve) => resolve({ data: Buffer.alloc(0), mimeType: '' }))

						file
							.then((result: any) => {
								result.data = result.data.toString('hex')
								worker.send({ type: 'request-file', result })
							})
							.catch(() => {
								worker.send({ type: 'request-file', result: Buffer.alloc(0) })
							})
						break

					case 'data':
						const data = this.sensorData[message.content.device]
						if (data)
							data['' + message.content.idSensor] = message.content.data
						else {
							const dat: any = { }
							dat['' + message.content.idSensor] = message.content.data

							this.sensorData[message.content.device] = dat
						}
						this.sensorData.dirty = true
						break

					// API Events
					default:
						this.fireEvent(message.type, message.content, worker.id)
						break
				}
		})


		// Methods
		ipcMain.on('virtual-pad-api-add-file', (event, file) => {
			switch (file.config.type) {
				case 'file':
				case 'url':
					this.files.set(file.name, file.config)
					break

				case 'folder':
					this.filesFolder.set(file.config.path, file.config)
					break

				case 'url-proxy':
					this.filesProxy.set(file.config.path, file.config)
					break

				case 'callback':
					this.callbacksFile.set(file.config.path, file.config)
					break
			}
		})

		ipcMain.on('virtual-pad-api-name', (event, name) => {
			this.sendMessageAll({ type: 'set-name', content: name })
		})

		ipcMain.on('virtual-pad-api-remove-file', (event, name) => {
			this.files.delete(name)
		})

		ipcMain.on('virtual-pad-api-remove-file-folder', (event, name) => {
			this.filesFolder.delete(name)
		})

		ipcMain.on('virtual-pad-api-remove-file-proxy', (event, name) => {
			this.filesProxy.delete(name)
		})

		ipcMain.on('virtual-pad-api-remove-file-callback', (event, name) => {
			this.callbacksFile.delete(name)
		})

		ipcMain.on('virtual-pad-api-data-frecuency', (event, frecuency) => {
			this.dataFrecuency = frecuency
			this.setWindow(this.win)
		})

		ipcMain.on('virtual-pad-api-set-converter', (event, message) => {
			this.sendMessageAll({ type: message.add ? 'set-converter' : 'remove-converter', content: {
				converter: message.converter,
				idSensor: message.idSensor,
			}})
		})

		ipcMain.on('virtual-pad-api-loaded', () => {
			for (const device of this.devices) {
				this.win.webContents.send('virtual-pad-api-connect', device)
				this.fireEvent('connect', device, undefined)
			}
		})

		ipcMain.on('virtual-pad-api-data', (event, message) => {
			if (message.device == null)
				this.sendMessageAll({ type: 'data', content: { device: null, idSensor: message.idSensor, data: message.data} })
			else
				for (const device of this.devices)
					if (device.address == message.device) {
						this.sendMessage(device.id, { type: 'data', content: { device: device.address, idSensor: message.idSensor, data: message.data} })
						break
					}
		})

		ipcMain.on('virtual-pad-api-send-file', (event, message) => {
			if (message.device == null)
				this.sendMessageAll({ type: 'send-file', content: { device: null, name: message.name, mimeType: message.mimeType, data: message.data} })
			else
				for (const device of this.devices)
					if (device.address == message.device) {
						this.sendMessage(device.id, { type: 'send-file',
							content: { device: device.address, name: message.name, mimeType: message.mimeType, data: message.data} })
						break
					}
		})

		ipcMain.on('virtual-pad-api-file-callback-response', (event, response) => {
			const resolve = this.callbacksResponses.get(response.name)
			if (resolve) {
				this.callbacksResponses.delete(response.name)
				resolve(response)
			}
		})

		ipcMain.on('config-backend-fullscreen', (event, message) => {
			this.win.setFullScreen(message)
		})
	}


	private fireEvent(type: string, data: any, workerId: number) {
		this.win.webContents.send('virtual-pad-api-event', { type, data, workerId })
		if (workerId != undefined)
			ipcMain.once('virtual-pad-api-event', (event, message) => {
				this.sendMessage(workerId, { type: message.type, result: message.result })
			})
	}


	public sendMessage(workerId: number, message: any) {
		const workers = Object.values(cluster.workers)
		for (const worker of workers)
			if (worker.id == workerId) {
				worker.send(message)
				break
			}
	}

	public sendMessageAll(message: any) {
		const workers = Object.values(cluster.workers)
		for (const worker of workers)
			worker.send(message)
	}

	public setWindow(win: BrowserWindow) {
		this.win = win

		if (this.dataSender)
			clearInterval(this.dataSender)

		if (win != null)
			this.dataSender = setInterval(() => {
				if (this.win && this.sensorData.dirty) {
					delete this.sensorData.dirty
					this.win.webContents.send('virtual-pad-api-data', this.sensorData)
					this.sensorData = { }
				}
			}, this.dataFrecuency)
	}

}
