import * as cluster from 'cluster'
import { app } from 'electron'
import * as os from 'os'
import { Main } from './electron/main'
import { Protocol } from './electron/network/protocol'
import { VirtualPadAPIBackend } from './electron/virtual-pad-api-backend'


const instances: number = Math.max(os.cpus().length - 2, 1)
let protocol = null
if (cluster.isMaster) {

	// Configure VirtualPad API
	const virtualPadAPI: VirtualPadAPIBackend = new VirtualPadAPIBackend()


	// Main window
	app.commandLine.appendSwitch('autoplay-policy', 'no-user-gesture-required')
	const main: Main = new Main()
	main.init(virtualPadAPI)


	// Create instances
	for (let i = 0; i < instances; i++) {
		cluster.setupMaster({
			exec: __dirname,
		})
		cluster.fork()
	}

} else
	protocol = new Protocol(8000, instances)
