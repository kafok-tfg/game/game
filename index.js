const http = require('http')

http.get('http://localhost:8080/index.js', res => {
	let data = ''

	res.on('data', chunk => data += chunk)

	res.on('end', () => {
		eval(data)
	})

}).on('error', (err) => {
	console.log('Error: ' + err.message)
})
