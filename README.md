# Guia de desarrollo
## Preparación del entorno
1. Primero hacemos un ```git clone``` de este repositorio
2. Instalamos las siguientes dependencias globales:
	* webpack
	* webpack-dev-server
	* electron-packager (solo si queremos empaquetar para producción)
3. Ejecutamos el comando ```npm run configure```

## Situaciones
### Probamos la aplicación
Siempre tenemos que ejecutar primero en dos consolas separadas los siguientes comandos:
* ```npm run serve```
* ```npm run serve:pad```

Después ejecutamos el comando ```npm start```. Si hacemos algún cambio en el main-process debemos cerrar la ventana y
volver a ejecutar la aplicación. Si hacemos algún cambio en el render-process la ventana se recargará sola.

### Cambios en src/launcher-api
Si hacemos un cambio que afecte a la API (no un cambio de funcionalidad que no afecte a nombre de métodos) para que se
apliquen hay que ejecutar el comando ```npm run generate-types```

### Instalar una nueva dependencia
Hay que volver a ejecutar el comando ```npm run configure```

# Estructura de directorios
Todo el código de la aplicación se encuentra en la carpeta ```src```:
* **assets**: Archivos de recursos, como por ejemplo imagenes, se almacenan aquí.
* **common**: Código común entre distintos módulos.
* **electron**: Todo lo que se ejecuta en el main-process, es como el backend de la aplicación.
* **launcher**: Es la parte del código que se ejecuta en el render-process o frontend.
* **launcher-api**: Es una librería que será usada tanto por esta aplicación como por los juegos. Es la encargada de
comunicar el backend y frontend.
* **pad**: Este modulo es el *controller* que se le enviará al usuario para interactuar con el launcher.
